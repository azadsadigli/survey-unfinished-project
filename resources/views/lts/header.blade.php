<header class="fixed-top head">
  <div class="container">
    <div class="logo float-left">
      <a href="/"><img src="img/logo.png" alt="Egim Survey" class="img-fluid"></a>
    </div>
    <nav class="main-nav float-right d-none d-lg-block">
      <ul>
        <li @if(Request::is('/')) class="active" @endif><a href="/">Home</a></li>
        <!-- <li class="drop-down"><a href="">Drop Down</a>
          <ul>
            <li class="drop-down"><a href="#">Drop Down 2</a>
              <ul>
                <li><a href="#">Deep Drop Down 1</a></li>
                <li><a href="#">Deep Drop Down 2</a></li>
                <li><a href="#">Deep Drop Down 3</a></li>
                <li><a href="#">Deep Drop Down 4</a></li>
                <li><a href="#">Deep Drop Down 5</a></li>
              </ul>
            </li>
            <li><a href="#">Drop Down 3</a></li>
          </ul>
        </li> -->
        @if(Auth::check())
        <li class="drop-down">
          <a href="#">My account</a>
          <ul>
            <li><a href="/home">My Profile</a> </li>
            <li> <a href="/create-survey">Create Survey</a> </li>
            <li><a href="/contact">Contact Us</a></li>
            <li><a class="dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form></li>
          </ul>
        </li>
        @else
        <li> <a href="/create-survey">Create Survey</a> </li>
        <li><a href="/contact">Contact Us</a></li>
        <li @if(Request::is('/register')) class="active" @endif><a href="/register">Register</a></li>
        <li @if(Request::is('/login')) class="active" @endif><a href="/login">Login</a></li>
        @endif
      </ul>
    </nav>
  </div>
</header>
