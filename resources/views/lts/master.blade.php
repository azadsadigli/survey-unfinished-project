<!DOCTYPE html>
<html lang="{{Config::get('app.locale')}}">
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  @section('head')
  @show
  <link href="/img/icon.png" rel="icon">
  <link href="/img/icon.png" rel="apple-touch-icon">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">
  <link href="/css/master.css" rel="stylesheet">
</head>
<body>
@include('lts.header')
@section('body')
@show
@include('lts.footer')
<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
<script src="/js/master.js"></script>
</body>
</html>
