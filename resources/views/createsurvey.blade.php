@extends('lts.master')
@section('body')
  <main id="main">
    <section class="section-bg main_section">
      <div class="container">
        <div class="row">
          <div class="col-md-12 offset-lg-1">
            <div class="box">
              <h4>Create Survey</h4><hr>
              <form>
                <div class="form-group">
                  <label for="">Text</label>
                  <textarea type="text" class="form-control" placeholder="Text" rows="5"></textarea>
                  <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div class="form-group">
                  <label for="">Text</label>
                  <input type="text" class="form-control" placeholder="Text">
                  <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div class="form-group">
                  <label for="">Text</label>
                  <input type="text" class="form-control" placeholder="Text">
                </div>
                <div class="form-check">
                  <input type="checkbox" class="form-check-input">
                  <label class="form-check-label" for="">Check me out</label>
                </div>
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
@endsection
