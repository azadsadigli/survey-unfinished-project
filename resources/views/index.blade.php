@extends('lts.master')
@section('head')
<title>{{__('app.Home')}} - EGIM Survey</title>
@endsection
@section('body')
  <section class="mainpart">
    <div class="container">
      <div class="intro-img"><img src="img/survey.png" alt="Egim Survey" class="img-fluid"></div>
      <div class="intro-info">
        <h2>We offer you<br><span>services</span> for<br> free!</h2>
        <div>
          <!-- <a href="#about" class="btn-get-started">Get Started</a> -->
          <a href="/create-survey" class="btn-services">{{__('app.Create_survey')}}</a>
        </div>
      </div>
    </div>
  </section>
  <main id="main">
    <section class="ourservicepart">
      <div class="container">
        <header class="section-header">
          <h3>{{__('app.What_we_offer')}}</h3>
          <!-- <p>Laudem latine persequeris id sed, ex fabulas delectus quo. No vel partiendo abhorreant vituperatoribus.</p> -->
        </header>
        <div class="row row-eq-height justify-content-center">
          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
                <i class="fa fa-diamond"></i>
              <div class="card-body">
                <h5 class="card-title">Corporis dolorem</h5>
                <p class="card-text">Deleniti optio et nisi dolorem debitis. Aliquam nobis est temporibus sunt ab inventore officiis aut voluptatibus.</p>
                <a href="#" class="readmore">{{__('app.Read_more')}}</a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
                <i class="fa fa-language"></i>
              <div class="card-body">
                <h5 class="card-title">Voluptates dolores</h5>
                <p class="card-text">Voluptates nihil et quis omnis et eaque omnis sint aut. Ducimus dolorum aspernatur.</p>
                <a href="#" class="readmore">{{__('app.Read_more')}}</a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 mb-4">
            <div class="card wow bounceInUp">
                <i class="fa fa-object-group"></i>
              <div class="card-body">
                <h5 class="card-title">Eum ut aspernatur</h5>
                <p class="card-text">Autem quod nesciunt eos ea aut amet laboriosam ab. Eos quis porro in non nemo ex. </p>
                <a href="#" class="readmore">{{__('app.Read_more')}}</a>
              </div>
            </div>
          </div>
        </div>
        <div class="row counters">
          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">{{100 + 2 * App\User::all()->count()}}</span>
            <p>Users</p>
          </div>
          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">421</span>
            <p>Survey</p>
          </div>
          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">1,364</span>
            <p>Hours Of Support</p>
          </div>
          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">18</span>
            <p>Hard Workers</p>
          </div>
        </div>
      </div>
    </section>
  </main>
@endsection
