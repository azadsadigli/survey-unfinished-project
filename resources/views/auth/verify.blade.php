@extends('lts.master')
@section('head')
<title>Login</title>
<meta content="" name="keywords">
<meta content="" name="description">
@endsection
@section('body')
  <main id="main">
    <section class="section-bg main_section">
      <div class="container">
        <div class="row">
          <div class="col-md-7 col-centered">
            <div class="box">
              <h4 class="">{{ __('Verify Your Email Address') }}</h4><hr>
              <div class="card-body">
                  @if (session('resent'))
                      <div class="alert alert-success" role="alert">
                          {{ __('A fresh verification link has been sent to your email address.') }}
                      </div>
                  @endif

                  {{ __('Before proceeding, please check your email for a verification link.') }}
                  {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
@endsection
